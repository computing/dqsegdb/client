#
# Copyright (C) 2006  Larne Pekowsky
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# This module was copied from the Grid LSC User Environment (GLUE) where
# it was distributed under the terms of GPL-3.0-or-later.
#

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#

"""
Utilities for working with segment databases

This was imported from lscsoft/glue@3c86d4402b2fc4cb2f1ee6dc84f7d2a45dbc8c65.
"""
